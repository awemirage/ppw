from django.shortcuts import render, HttpResponseRedirect, get_object_or_404
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.urls import  reverse_lazy
from datetime import datetime, date
from .forms import Add_Schedule, Add_Contact    
from .models import Contact, Schedule

# Create your views here.

def index(request):
    response = {}
    return render(request, 'lab_1/index.html', response)

def about_me(request):
    response = {}
    return render(request, 'lab_1/about_me.html', response)

def contact_me(request):
    form = Add_Contact(request.POST or None)
    response = {'form_contact' : Add_Contact, 'success' : '',}
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['email'] = request.POST['email']
        response['message'] = request.POST['message']
        response['success'] = "success"
        contact = Contact(name=response['name'], email=response['email'], message=response['message'])
        contact.save()
        return render(request, 'lab_1/contact_me.html', response)
    else:
        return render(request, 'lab_1/contact_me.html', response)

    
def portfolio(request):
    return render(request, 'lab_1/portfolio.html', {})

def thank_you(request):
    return render(request, 'lab_1/thank_you.html', {})

def portfolio_ex(request):
    return render(request, 'lab_1/portfolio_ex.html', {})

def schedule(request):
    schedule = Schedule.objects.all().values()
    response = {'list_schedule' : schedule}
    if(request.method == 'POST'):
        for i in request.POST :
            print(i)
    return render(request, 'lab_1/schedule.html', response)

def add_schedule(request):
    form = Add_Schedule(request.POST or None)
    response = {'form_schedule' : Add_Schedule}
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['date'] = request.POST['date']
        response['time'] = request.POST['time']
        response['place'] = request.POST['place']
        response['category'] = request.POST['category']
        schedule = Schedule(name=response['name'], date=response['date'], time=response['time'], place=response['place'], category=response['category'])
        schedule.save()
    return render(request, 'lab_1/add_schedule.html', response)

def delete_schedule(request, schedule_name):
    schedule_1 = Schedule.objects.get(name = schedule_name)
    response = {'schedule_name' : str(schedule_1.name)}
    schedule_1.delete()
    return render(request, 'lab_1/delete_schedule.html', response)