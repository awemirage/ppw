from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from . import views
from .views import *

app_name='lab_1'
urlpatterns = [
    path('', views.index, name='index'),
    path('about_me/', views.about_me, name='about_me'),
    path('contact_me/', views.contact_me, name='contact_me'),
    path('contact_me/thank_you/', views.thank_you, name='thank_you'),
    path('portfolio/', views.portfolio, name='portfolio'),
    path('portfolio/example/', views.portfolio_ex, name='portfolio_ex'),
    path('schedule/', views.schedule, name='schedule'),
    path('schedule/add_schedule/', views.add_schedule, name='add_schedule'),
    path('schedule/delete/<schedule_name>)', views.delete_schedule, name='delete_schedule')
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.STATIC_URL, document_root=settings.STATICFILES_DIRS)
