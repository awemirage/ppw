from django.test import TestCase, Client
from django.urls import resolve, reverse
import unittest
from .views import index, about_me, contact_me, portfolio, thank_you, portfolio_ex, schedule, add_schedule
from .forms import Add_Schedule, Add_Contact
from .models import Contact, Schedule

# Create your tests here.
class Lab1Test(TestCase):
    #urls
    def test_index_url_exists(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)

    def test_about_me_url_exists(self):
        response = self.client.get('/about_me/')
        self.assertEqual(response.status_code, 200)
    
    def test_contact_me_url_exists(self):
        response = self.client.get('/contact_me/')
        self.assertEqual(response.status_code, 200)

    def test_thank_you_url_exists(self):
        response = self.client.get('/contact_me/thank_you/')
        self.assertEqual(response.status_code, 200)

    def test_portfolio_url_exists(self):
        response = self.client.get('/portfolio/')
        self.assertEqual(response.status_code, 200)

    def test_portfolio_ex_url_exists(self):
        response = self.client.get('/portfolio/example/')
        self.assertEqual(response.status_code, 200)

    def test_schedule_url_exists(self):
        response = self.client.get('/schedule/')
        self.assertEqual(response.status_code, 200)

    def test_add_schedule_url_exists(self):
        response = self.client.get('/schedule/add_schedule/')
        self.assertEqual(response.status_code, 200)

    #template
    def test_index_template(self):
        response = self.client.get('')
        self.assertTemplateUsed(response, 'lab_1/index.html')

    def test_about_me_template(self):
        response = self.client.get('/about_me/')
        self.assertTemplateUsed(response, 'lab_1/about_me.html')
    
    def test_contact_me_template(self):
        response = self.client.get('/contact_me/')
        self.assertTemplateUsed(response, 'lab_1/contact_me.html')

    def test_thank_you_template(self):
        response = self.client.get('/contact_me/thank_you/')
        self.assertTemplateUsed(response, 'lab_1/thank_you.html')

    def test_portfolio_template(self):
        response = self.client.get('/portfolio/')
        self.assertTemplateUsed(response, 'lab_1/portfolio.html')

    def test_portfolio_ex_template(self):
        response = self.client.get('/portfolio/example/')
        self.assertTemplateUsed(response, 'lab_1/portfolio_ex.html')

    def test_schedule_template(self):
        response = self.client.get('/schedule/')
        self.assertTemplateUsed(response, 'lab_1/schedule.html')

    def test_add_schedule_template(self):
        response = self.client.get('/schedule/add_schedule/')
        self.assertTemplateUsed(response, 'lab_1/add_schedule.html')

    #models
    def test_contact_model(self):
        contact_test = Contact(name='Timothy Wimbledon', email='Timothy@neverwinter.com', message='I am the king')
        self.assertEqual(str(contact_test), contact_test.name)

    def test_schedule_model(self):
        schedule_test = Schedule(name='Timothy Wimbledon', date='2019-10-16', time='00:00', place='Neverwinter', category='Outting')
        self.assertEqual(str(schedule_test), schedule_test.name)

# HTTP Statuses
# 2** = OK
# 3** = Movement (redirect, etc)
# 4** = User end error
# 5** = internal server error
